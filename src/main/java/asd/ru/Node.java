package asd.ru;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "node")
@XmlRootElement
@Entity
@Table(name = "node")
public class Node {
    @XmlAttribute
    @Id
    protected long id;

    @XmlAttribute
    protected double lat;

    @XmlAttribute
    protected double lon;

    public long getId() {
        return id;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    @XmlTransient
    public void setId(long id) {
        this.id = id;
    }

    @XmlTransient
    public void setLat(double lat) {
        this.lat = lat;
    }

    @XmlTransient
    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "asd.ru.Node{" +
                "id=" + id +
                ", lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
