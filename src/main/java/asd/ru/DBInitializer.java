package asd.ru;

import java.sql.*;

public class DBInitializer {
    static final String DROP_NODE = "DROP TABLE IF EXISTS node";

    static final String CREATE_NODE = "CREATE TABLE node (\n" +
            "    id        bigint CONSTRAINT firstkey PRIMARY KEY,\n" +
            "    lat       double precision NOT NULL,\n" +
            "    lon       double precision NOT NULL\n" +
            ");";

    static void initialize() {
        try (Connection connection = ConnectionFactory.getConnection()) {
            Statement st = connection.createStatement();
            st.execute(DROP_NODE);
            st.execute(CREATE_NODE);
            st.close();
        } catch (SQLException e) {
            System.out.println("Initialization failed");
            e.printStackTrace();
        }
    }
}
