package asd.ru;

import java.sql.*;
import java.util.List;

abstract public class NodeDAO {
    static private Node extractNodeFromResultSet(ResultSet rs) throws SQLException {
        Node user = new Node();
        user.setId( rs.getInt("id") );
        user.setLat( rs.getDouble("lat") );
        user.setLon( rs.getDouble("lon") );
        return user;
    }

    static public Node getNode(int id) {
        try (Connection connection = ConnectionFactory.getConnection()) {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM node WHERE id=" + id);
            if(rs.next())
            {
                return extractNodeFromResultSet(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    static public boolean insertNode(Node node) {
        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO node VALUES (?, ?, ?)");
            ps.setLong(1, node.getId());
            ps.setDouble(2, node.getLat());
            ps.setDouble(3, node.getLon());
            int i = ps.executeUpdate();
            ps.close();
            if(i == 1) {
                return true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    static public void insertNodes_query(List<Node> nodes) {
        try (Connection connection = ConnectionFactory.getConnection()) {
            Statement st = connection.createStatement();
            for (Node n : nodes) {
                st.executeUpdate("INSERT INTO node (id, lat, lon) VALUES (" + n.getId() +", " + n.getLat() + ", " + n.getLon() + ")");
            }
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings("Duplicates")
    static public void insertNodes_prepared(List<Node> nodes) {
        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO node (id, lat, lon) VALUES (?, ?, ?)");
            for (Node n : nodes) {
                ps.setLong(1, n.getId());
                ps.setDouble(2, n.getLat());
                ps.setDouble(3, n.getLon());
                ps.executeUpdate();
            }
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    static public void insertNodes_batch(List<Node> nodes) {
//        final int batchSize = 10000;
//        int count = 0;
        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO node (id, lat, lon) VALUES (?, ?, ?)");
            for (Node n : nodes) {
                ps.setLong(1, n.getId());
                ps.setDouble(2, n.getLat());
                ps.setDouble(3, n.getLon());
                ps.addBatch();
//                if(++count % batchSize == 0) {
//                    ps.executeBatch();
//                }
            }
            ps.executeBatch();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
