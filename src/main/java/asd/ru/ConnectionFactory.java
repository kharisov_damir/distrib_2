package asd.ru;

import java.sql.*;

public class ConnectionFactory {
    static final String URL = "jdbc:postgresql://127.0.0.1:5432/test";
    static final String USER = "postgres";
    static final String PASS = "1234";

    public static Connection getConnection() {
        try {
            return DriverManager.getConnection(URL, USER, PASS);
        } catch (SQLException ex) {
            throw new RuntimeException("Error connecting to the database", ex);
        }
    }
}
