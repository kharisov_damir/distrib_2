package asd.ru;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class ProcessNVS {
    static public void CountUsers(BZip2CompressorInputStream bzStream) throws IOException, javax.xml.stream.XMLStreamException {
        try (StaxStreamProcessor processor = new StaxStreamProcessor(bzStream)) {
            Map<String, Integer> map = new HashMap<>();
            XMLStreamReader reader = processor.getReader();
            while (reader.hasNext()) {       // while not end of XML
                int event = reader.next();   // read next event
                if (event == XMLEvent.START_ELEMENT &&
                        "node".equals(reader.getLocalName())) {
                    String user = reader.getAttributeValue(reader.getNamespaceURI(), "user");
                    if (map.containsKey(user)) {
                        map.compute(user, (k, v) -> v + 1);
                    } else {
                        map.put(user, 1);
                    }
                }
            }
            Map<String, Integer> result = map.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
            for (String key : result.keySet()){
                String value = map.get(key).toString();
                System.out.println(key + " " + value);
            }
        }
    }
}
