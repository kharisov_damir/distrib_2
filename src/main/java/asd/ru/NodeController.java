package asd.ru;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NodeController {
    @Autowired
    public NodeCrudRepository nodeCrudRepository;

    @GetMapping(value = "/node/{id}")
    public Node getNode(@PathVariable Long id) {
        Node n = nodeCrudRepository.findById(id).get();
        return n;
    }

    @GetMapping(value = "/nodes_in_rad")
    public String getNodesInRadius(@RequestParam(value = "lat") Double lat,
                                   @RequestParam(value = "lon") Double lon,
                                   @RequestParam(value = "rad") Long rad) {
        List nodes = nodeCrudRepository.getInRadius(lat, lon, rad);
        return new Gson().toJson(nodes);
    }
}
