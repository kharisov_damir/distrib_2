package asd.ru;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class Main {
    final static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Main.class, args);
//        initDb();
    }

    static public void initDb() throws Exception {
        Main.logger.info("Hello world");
        InputStream is = Files.newInputStream(Paths.get("C:\\Users\\dkharisov\\IdeaProjects\\distrib\\src\\main\\resources\\RU-NVS.osm.bz2"));
        BZip2CompressorInputStream bzStream = new BZip2CompressorInputStream(is);
        //asd.ru.ProcessNVS.CountUsers(bzStream);
        BufferedReader reader = new BufferedReader(new InputStreamReader(bzStream));
        JAXBContext context = JAXBContext.newInstance(Osm.class, Node.class);
        List l = new ArrayList<Node>();
        Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setListener(new Unmarshaller.Listener() {
            @Override
            public void afterUnmarshal(Object target, Object parent) {
                super.afterUnmarshal(target, parent);
                if (target instanceof Node) l.add(target);
                if (l.size() % 10000 == 0) {
                    NodeDAO.insertNodes_batch(l);
                    l.clear();
                }
            }
        });
        logger.info("Initializing..");
        DBInitializer.initialize();
        Main.logger.info("Unmarshalling..");
        unmarshaller.unmarshal(reader);
        NodeDAO.insertNodes_batch(l);
//        Main.logger.info("Inserting..");
//        NodeDAO.insertNodes_batch(osm.nodes);
//        Main.logger.info("finish");
//        measureInserts(osm.nodes);
    }

    static private void measureInserts(List<Node> nodes) {
        DBInitializer.initialize();
        long startTime = System.currentTimeMillis();
        NodeDAO.insertNodes_query(nodes);
        long endTime = System.currentTimeMillis();
        System.out.println(String.format("Query %d min, %d sec, %d milis",
                TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                TimeUnit.MILLISECONDS.toSeconds(endTime - startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime)),
                endTime-startTime - TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(endTime-startTime))
        ));
        System.out.println("Rate: " + ((double) nodes.size()/(endTime-startTime))*1000 + " ops/sec");
        DBInitializer.initialize();
        startTime = System.currentTimeMillis();
        NodeDAO.insertNodes_prepared(nodes);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Prepared %d min, %d sec, %d milis",
                TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                TimeUnit.MILLISECONDS.toSeconds(endTime - startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime)),
                endTime-startTime - TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(endTime-startTime))
        ));
        System.out.println("Rate: " + ((double) nodes.size()/(endTime-startTime))*1000 + " ops/sec");
        DBInitializer.initialize();
        startTime = System.currentTimeMillis();
        NodeDAO.insertNodes_batch(nodes);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Batch %d min, %d sec, %d milis",
                TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                TimeUnit.MILLISECONDS.toSeconds(endTime - startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime)),
                endTime-startTime - TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(endTime-startTime))
        ));
        System.out.println("Rate: " + ((double) nodes.size()/(endTime-startTime))*1000 + " ops/sec");
    }
}

//Query 24 min, 30 sec, 709 milis
//Rate: 3402.1862924616635 ops/sec
//Prepared 20 min, 53 sec, 923 milis
//Rate: 3990.377399569192 ops/sec
//Batch 1 min, 10 sec, 982 milis
//Rate: 70491.47671240596 ops/sec

//22:25:38.376 [main] INFO asd.ru.Main - Hello world
//        22:25:38.908 [main] INFO asd.ru.Main - Unmarshalling..
//        22:29:11.719 [main] INFO asd.ru.Main - Initializing..
//        22:29:11.914 [main] INFO asd.ru.Main - Inserting..
//        22:31:02.346 [main] INFO asd.ru.Main - finish