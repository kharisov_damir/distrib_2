package asd.ru;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlType(name = "Osm")
@XmlRootElement
public class Osm {
    @XmlElement(name = "node")
    public List<Node> nodes = new ArrayList<>();
}
