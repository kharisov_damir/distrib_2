package asd.ru;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NodeCrudRepository extends CrudRepository<Node, Long> {
    @Query(value = "select * from (select *, earth_distance(ll_to_earth(node.lat, node.lon), ll_to_earth(:lat, :lon))" +
            " as dist from node) as sub where dist < :rad order by dist;",
            nativeQuery = true)
    List<Node> getInRadius(@Param("lat") Double lat, @Param("lon") Double lon, @Param("rad") Long radius);
}